/** @format */

import React, { PureComponent } from "react";
import {
  Text,
  TextInput,
  FlatList,
  View,
  TouchableOpacity,
  I18nManager,
  Animated,
} from "react-native";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";

import { Color, Constants, Icons, Languages, withTheme } from "@common";
import { FlatButton, Spinkit, ProductItem, AnimatedHeader } from "@components";
import { BlockTimer, warn } from "@app/Omni";
import styles from "./styles";

import SearchBar from "./SearchBar";
import Recents from "./Recents";

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

class Search extends PureComponent {
  constructor(props) {
    super(props);
    this.page = 1;
    this.limit = Constants.pagingLimit;
    this.state = {
      text: "",
      isSubmit: false,
      loading: false,
      focus: true,
      scrollY: new Animated.Value(0),
      filter: {},
    };
  }

  scrollY = new Animated.Value(0);

  onBack = () => {
    this.setState({ text: "" });
    Keyboard.dismiss();
    this.props.onBack();
  };

  startNewSearch = async () => {
    const { list } = this.props;
    this.setState({ loading: true, isSubmit: true });
    await this.props.fetchProductsByName(
      this.state.text,
      this.limit,
      this.page
    );
    if (typeof list !== "undefined") {
      this.setState({ loading: false });
    }
  };

  onRowClickHandle = (product) => {
    BlockTimer.execute(() => {
      this.props.onViewProductScreen({ product });
    }, 500);
  };

  renderItem = ({ item }) => {
    return (
      <ProductItem
        small
        product={item}
        onPress={() => this.onRowClickHandle(item)}
      />
    );
  };

  nextPosts = () => {
    this.page += 1;
    this.props.fetchProductsByName(this.state.text, this.limit, this.page);
  };

  renderHeader = () => {
    const {
      theme: {
        colors: { background, text },
      },
      navigation,
    } = this.props;

    return (
      <View>
        <Recents
          histories={this.props.histories}
          searchText={this.state.text}
          onClear={this.props.clearSearchHistory}
          onSearch={this.onSearch}
        />
      </View>
    );
  };
  renderResultList = () => {
    const { list, isFetching } = this.props;
    const { isSubmit } = this.state;

    const onScroll = Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: {
              y: this.state.scrollY,
            },
          },
        },
      ],
      { useNativeDriver: true }
    );

    return list !== "" ? (
      <AnimatedFlatList
        keyExtractor={(item, index) => `${item.id} || ${index}`}
        contentContainerStyle={styles.flatlist}
        data={list}
        scrollEventThrottle={1}
        renderItem={this.renderItem}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={() => {
          return list.length > 20 ? (
            <View style={styles.more}>
              <FlatButton
                name="arrow-down"
                text={isFetching ? "LOADING..." : "MORE"}
                load={this.nextPosts}
              />
            </View>
          ) : null;
        }}
        {...{ onScroll }}
      />
    ) : (
      isSubmit &&
        !isFetching && (
          <Text style={{ textAlign: "center" }}>{Languages.NoResultError}</Text>
        )
    );
  };

  render() {
    const {
      theme: {
        colors: { background, text },
      },
    } = this.props;

    return (
      <View style={[styles.container, {backgroundColor: background} ]}>
        <SearchBar
          scrollY={this.state.scrollY}
          autoFocus={this.state.focus}
          value={this.state.text}
          onChangeText={(text) => this.setState({ text })}
          onSubmitEditing={this.searchProduct}
          onClear={() => this.setState({ text: "" })}
          onFilter={() => this.props.onFilter(this.onFilter)}
          isShowFilter={this.state.text != "" || this.props.list.length > 0}
          haveFilter={Object.keys(this.state.filter).length > 0}
        />


        {this.props.isFetching ? <Spinkit /> : this.renderResultList()}
      </View>
      // <InstantSearch />
    );
  }

  searchProduct = () => {
    this.props.saveSearchHistory(this.state.text);
    this.startNewSearch();
  };

  onSearch = (text) => {
    this.setState({ text }, this.searchProduct);
  };

  onFilter = async (filter) => {
    const { list } = this.props;
    this.setState({ loading: true, isSubmit: true, filter });
    await this.props.filterProducts(
      this.state.text,
      this.limit,
      this.page,
      filter
    );
    if (typeof list !== "undefined") {
      this.setState({ loading: false });
    }
  };
}

Search.defaultProps = {
  histories: [],
};

const mapStateToProps = ({ products }) => ({
  list: products.productsByName,
  isFetching: products.isFetching,
  histories: products.histories,
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/ProductRedux");
  return {
    ...ownProps,
    ...stateProps,
    fetchProductsByName: (name, per_page, page, filter = {}) => {
      if (name.length > 0) {
        actions.fetchProductsByName(dispatch, name, per_page, page, filter);
      }
    },
    saveSearchHistory: (searchText) => {
      if (searchText.length > 0) {
        actions.saveSearchHistory(dispatch, searchText);
      }
    },
    clearSearchHistory: () => {
      actions.clearSearchHistory(dispatch);
    },
    filterProducts: (name, per_page, page, filter = {}) => {
      actions.fetchProductsByName(dispatch, name, per_page, page, filter);
    },
  };
};
module.exports = connect(
  mapStateToProps,
  null,
  mergeProps
)(withTheme(Search));
